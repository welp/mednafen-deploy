# mednafen-server container and deployment scripts

## dockerfile

to build:

```
docker build . -t mednafen-server:0.5.2 # or latest version
```

to run locally:

```
docker run -p 4046:4046 --env-file=./.env mednafen-server:0.5.2
```

edit `.env` as appropriate.

## chart

1. create a custom values file or modify `chart/values.yaml`
1. run e.g. `helm install [-n namespace] mednafen-server ./chart [-f extra-values.yaml]`
1. open port 4046 on the cluster. if you’re using the community
   [`ingress-nginx`](https://github.com/kubernetes/ingress-nginx/), that means
   adding a `tcp` block to your ingress deployment (replacing
   `mednafen-svc-name` with the name of the `Service` object in your deployment):
   ```
   tcp:
      4046: "namespace/mednafen-svc-name:4046"
   ```
