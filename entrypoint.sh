#!/bin/sh

# shellcheck disable=SC2016
envs='
${CONNECT_TIMEOUT}
${IDLE_TIMEOUT}
${MAX_CLIENTS}
${MAXCMDPAYLOAD}
${MAXSENDQSIZE}
${MINSENDQSIZE}
${PORT}
'

envsubst "${envs}" < /mednafen/standard.conf.template > /mednafen/standard.conf

if [ -z "${PASSWORD}" ]; then
  echo '-- no password set for server!'
else
  echo "password ${PASSWORD}" >> /mednafen/standard.conf
fi

exec "$@"
