FROM alpine:3.17 as build
ARG SERVER_VER=0.5.2

RUN apk --no-cache add \
  build-base \
  gcompat \
  curl \
  xz

RUN mkdir -p /build/compiled
WORKDIR /build

RUN curl -OL https://mednafen.github.io/releases/files/mednafen-server-${SERVER_VER}.tar.xz
RUN unxz mednafen-server-${SERVER_VER}.tar.xz
RUN tar xvf mednafen-server-${SERVER_VER}.tar

WORKDIR /build/mednafen-server

RUN ./configure --prefix=/build/compiled
RUN make install

FROM alpine:3.17 as run

RUN apk --no-cache add \
  gettext \
  libstdc++ \
  tini

ARG GID=10001
ARG UID=10000

COPY --from=build /build/compiled/bin/mednafen-server /mednafen/server

COPY standard.conf /mednafen/standard.conf.template
COPY entrypoint.sh /mednafen/entrypoint.sh

RUN addgroup -S --gid $GID app && \
  adduser -S -G app -u $UID -s /bin/sh -h /app app
RUN chown -R $UID:$GID /mednafen

USER app

ENTRYPOINT ["tini", "--", "/mednafen/entrypoint.sh"]
CMD ["/mednafen/server", "/mednafen/standard.conf"]
